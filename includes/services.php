<section class="ftco-section" id="services">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
            <h2>Nos services &amp; Avantages</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services p-3 py-4 d-block text-center">
                <div class="icon mb-3"><span class="flaticon-maintenance"></span></div>
                <div class="media-body">
                <h3 class="heading">Gestion de chantier</h3>
                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
                </div>
            </div>      
            </div>
            <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services p-3 py-4 d-block text-center">
                <div class="icon mb-3"><span class="flaticon-construction"></span></div>
                <div class="media-body">
                <h3 class="heading">Réhabilitation de biens</h3>
                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
                </div>
            </div>      
            </div>
            <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services p-3 py-4 d-block text-center">
                <div class="icon mb-3"><span class="flaticon-conveyor"></span></div>
                <div class="media-body">
                <h3 class="heading">Chantiers Neufs</h3>
                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
                </div>
            </div>    
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services p-3 py-4 d-block text-center">
                <div class="icon mb-3"><span class="flaticon-engineer"></span></div>
                <div class="media-body">
                <h3 class="heading">Maintenance de batîments</h3>
                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
                </div>
            </div>      
            </div>
        </div>
    </div>
</section>