<section class="ftco-section contact-section">
      <div class="container bg-light">
        <div class="row d-flex mb-5 contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h4">Infos Contact</h2>
          </div>
          <div class="w-100"></div>
          <div class="col-md-3">
            <p><span>Adresse:</span> 7 boulevard François René de Châteaubriand, 77000 Melun</p>
          </div>
          <div class="col-md-3">
            <p><span>Phone:</span> <a href="tel://1234567920">+33 (6) 60 60 60 60</a></p>
          </div>
          <div class="col-md-3">
            <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
          </div>
          <div class="col-md-3">
            <p><span>Website</span> <a href="#">yoursite.com</a></p>
          </div>
        </div>
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
            <form action="#">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Votre nom">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Votre adresse mail">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Sujet">
              </div>
              <div class="form-group">
                <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" value="Envoyer" class="btn btn-primary py-3 px-5">
              </div>
            </form>
          
          </div>

          <div class="col-md-6" >
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2640.9891227669505!2d2.6450205156636213!3d48.55260147925876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e5faf3c24fb47b%3A0x5845e8ca65647a88!2s7+Boulevard+Fran%C3%A7ois+Ren%C3%A9+de+Ch%C3%A2teaubriand%2C+77000+Melun!5e0!3m2!1sen!2sfr!4v1561554653376!5m2!1sen!2sfr" frameborder="0" style="border:0; width:100%; height:100%;" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </section>