<section class="ftco-section bg-light" id="realisations">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5 ftco-animate">
            <div class="col-md-7 text-center heading-section">
            <h2>Nous aimons partager nos réalisations</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 ftco-animate">
            <a href="images/image_1.jpg" class="project project-img d-flex justify-content-center image-popup" style="background-image: url(images/image_1.jpg);">
                <div class="align-self-center icon">
                <span class="ion-ios-search"></span>
                </div>
            </a>
            </div>
            <div class="col-md-6">
            <div class="row">
                <div class="col-sm-12 ftco-animate">
                <a href="images/image_2.jpg" class="project project-img project-img-2 d-flex justify-content-center image-popup" style="background-image: url(images/image_2.jpg);">
                    <div class="align-self-center icon">
                    <span class="ion-ios-search"></span>
                    </div>
                </a>
                </div>
                <div class="col-sm-12 ftco-animate">
                <a href="images/image_3.jpg" class="project project-img project-img-2 d-flex justify-content-center image-popup" style="background-image: url(images/image_3.jpg);">
                    <div class="align-self-center icon">
                    <span class="ion-ios-search"></span>
                    </div>
                </a>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>