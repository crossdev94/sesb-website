<section class="ftco-section-2">
      <div class="container">
        <div class="row pt-5">
          <div class="col-md-12 text-center mb-5 pt-5" data-aos="fade-up">
            <h1 class="ftco-heading heading-thin mb-5">Fort de nos récentes expériences dans le services aux particuliers et aux entreprises,
                nous avons cumulés un taux de verdicts positifs de 98%. Ces stats sont encoureageantes et temoigne aussi d'un gage de qualité de notre part.
            </h1>
          </div>
        </div>
      </div>
      <div>
        <div class="section-2-blocks-wrapper row no-gutters">
          <div class="img col-sm-12 col-md-6" style="background-image: url('images/image_4.jpg');" data-aos="fade-right">
            <a href="https://vimeo.com/45830194" class="button popup-vimeo" data-aos="fade-right" data-aos-delay="700"><span class="ion-ios-play"></span></a>
          </div>
          <div class="text col-md-6">
            <div class="text-inner align-self-start" data-aos="fade-up">
              
              <h3>Fondée en 2019, Notre équipe est composé de professionnels aguérris</h3>
              <p>Tous ces professionels se sont réunis dans le but d'améliorer la qualité des services sur le marché. Une entrprise à l'origine de Cedric LITCHE, Fondateur de la societé.</p>

              <p>Découvrez en video l'histoire de notre société, vous y apprendrais quelle sont nos sources de motivations.</p>
            </div>
          </div>
        </div>
      </div>
    </section>