<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md">
          <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2"><b>Nom de la Marque</b></h2>
            <p>"Ce qui coûte le plus cher dans une construction ce sont les erreurs."<br>
            <i>Ken Follett</i></p>
            
          </div>
        </div>
        <div class="col-md">
           <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Liens Rapides</h2>
            <ul class="list-unstyled">
              <li><a href="#" class="py-2 d-block">A propos</a></li>
              <li><a href="#" class="py-2 d-block">Nos projets</a></li>
              <li><a href="#" class="py-2 d-block">Que faisons-nous ?</a></li>
              <li><a href="#" class="py-2 d-block">Actualités</a></li>
              <li><a href="#" class="py-2 d-block">Contact</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md">
           <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Infos Contact</h2>
            <ul class="list-unstyled">
              <li><a href="#" class="py-2 d-block">7 boulevard François René de Châteaubriand, 77000 Melun</a></li>
              <li><a href="#" class="py-2 d-block">+33 (6) 60 60 60 60</a></li>
              <li><a href="#" class="py-2 d-block">info@yoursite.com</a></li>
              <li><a href="#" class="py-2 d-block">email@email.com</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md">
          <div class="ftco-footer-widget mb-4">
            <ul class="ftco-footer-social list-unstyled float-md-right float-lft">
              <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
              <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
              <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">

          <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Site developpé par <a href="#" target="_blank">CrossDev - Agence Digitale</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
        </div>
      </div>
    </div>
  </footer>