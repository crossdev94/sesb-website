<section class="ftco-section bg-light" id="prix">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
        <div class="col-md-7 text-center heading-section ftco-animate">
            <h2>Nos Prix</h2>
        </div>
        </div>
        <div class="row">
        <div class="col-md-4 mb-3 text-center ftco-animate">
            <div class="pricing p-4 py-5">
            <div class="icon mb-2"><span class="flaticon-skyline"></span></div>
            <h3 class="mb-3">Basic Plan</h3>
            <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            <p class="price mb-4 py-3"><span class="mr-2 currency">$29</span> <span class="per">per month</span></p>
            <p><a href="#" class="btn btn-primary">Get started</a></p>
            <a href="#">Open Source</a>
            </div>
        </div>
        <div class="col-md-4 mb-3 text-center ftco-animate">
            <div class="pricing p-4 py-5">
            <div class="icon mb-2"><span class="flaticon-skyline"></span></div>
            <h3 class="mb-3">Standard Plan</h3>
            <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            <p class="price mb-4 py-3"><span class="mr-2 currency">$29</span> <span class="per">per month</span></p>
            <p><a href="#" class="btn btn-primary">Get started</a></p>
            <a href="#">Free 30 Day Trial</a>
            </div>
        </div>
        <div class="col-md-4 mb-3 text-center ftco-animate">
            <div class="pricing p-4 py-5">
            <div class="icon mb-2"><span class="flaticon-skyline"></span></div>
            <h3 class="mb-3">Premium Plan</h3>
            <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            <p class="price mb-4 py-3"><span class="mr-2 currency">$29</span> <span class="per">per month</span></p>
            <p><a href="#" class="btn btn-primary">Get started</a></p>
            <a href="#" data-toggle="modal" data-target="#modalRequest">Request a Quote</a>
            </div>
        </div>
        </div>
    </div>
</section>