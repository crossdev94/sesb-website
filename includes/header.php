<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar" data-aos="fade-down" data-aos-delay="500">
    <div class="container">
    <a class="navbar-brand" href="index.php">Logo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item" name="accueil"><a href="index.php" class="nav-link">Accueil</a></li>
            <li class="nav-item" name="services"><a href="#services" class="nav-link">Que Faisons-nous</a></li>
            <li class="nav-item" name="realisations"><a href="#realisations" class="nav-link">Nos projets</a></li>
            <li class="nav-item" name="prix"><a href="#prix" class="nav-link">Nos Prix</a></li>
            <li class="nav-item" name="actualites"><a href="#actualites" class="nav-link">Actualités</a></li>
            <li class="nav-item"><a href="index.php?about" class="nav-link">A propos</a></li>
            <li class="nav-item"><a href="index.php?contact" class="nav-link">Contact</a></li>
        </ul>
    </div>
    </div>
</nav>
<!-- END nav -->