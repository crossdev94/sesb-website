<section class="ftco-section cover_1" style="background-image: url(images/bg_3.jpg);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row text-center justify-content-center">
            <div class="col-md-8 ftco-animate">
            <h2 class="heading">Un besoin spécifiques ? Faîtes nous le savoir !</h2>
            <p class="sub-heading mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi unde impedit, necessitatibus, soluta sit quam minima expedita atque corrupti reiciendis.</p>
            <p><a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalRequest">Demandez un devis</a></p>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="modalRequest" tabindex="-1" role="dialog" aria-labelledby="modalAppointmentLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modalAppointmentLabel">Demander un devis</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="#">
                    <div class="form-group">
                    <label for="appointment_name" class="text-black">Nom Prenom</label>
                    <input type="text" class="form-control" id="appointment_name">
                    </div>
                    <div class="form-group">
                    <label for="appointment_email" class="text-black">Email</label>
                    <input type="text" class="form-control" id="appointment_email">
                    </div>
                    <div class="form-group">
                    <label for="appointment_email" class="text-black">Sujet</label>
                    <input type="text" class="form-control" id="appointment_sujet">
                    </div>
                    <div class="form-group">
                    <label for="appointment_message" class="text-black">Message</label>
                    <textarea name="" id="appointment_message" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                    <input type="submit" value="Demander un devis" class="btn btn-primary">
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>