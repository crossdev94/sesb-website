<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('images/bg_1.jpg');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row slider-text align-items-center">
        <div class="col-md-7 col-sm-12 ftco-animate">
            <p>Nous sommes<strong><a href="#">&nbsp; Vote Société</a></strong>, Rachat, Chantier et Construction</p>
            <h1 class="mb-3">Nous donnons vie à vos projet les plus complexes</h1>
        </div>
        </div>
    </div>
    </div>

    <div class="slider-item" style="background-image: url('images/bg_2.jpg');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row slider-text align-items-center">
        <div class="col-md-7 col-sm-12 ftco-animate">
            <p><strong><a href="#">Vote Société</a></strong>, Expertise et Analyse</p>
            <h1 class="mb-3">Notre savoir a votre service</h1>
        </div>
        </div>
    </div>
    </div>
</section>
<!-- END slider -->