

var topS = $("#services").offset().top;
var topP = $("#realisations").offset().top;
var topPx = $("#prix").offset().top;
var topA = $("#actualites").offset().top;

$(document).scroll(function(){
    var scrollPos =  $(document).scrollTop();
    if( scrollPos < topS){
        activeHeader('accueil');
    }
    else if( scrollPos >= topS && scrollPos < topP){
        activeHeader('services');
    }
    else if(scrollPos >= topP && scrollPos < topPx){
        activeHeader('realisations');
    }
    else if(scrollPos >= topPx && scrollPos < topA){
        activeHeader('prix');
    }
    else if(scrollPos >= topA){
        activeHeader('actualites');
    }
});

function activeHeader(nom){
    var items = document.getElementsByClassName('nav-item');
    for (var i = 0; i < items.length; i++) {   
        if(items[i].classList.contains("active")){
            items[i].classList.remove("active");
        }
    }

    for (var i = 0; i < items.length; i++) {   
        if(items[i].getAttribute("name")== nom){
            items[i].classList.add("active");
        }
    }    
}